using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using VisitList.Models;
using VisitList.Services;

namespace Visits.Pages
{
    public class VisitsModel : PageModel
    {
         public List<Visit> visitList = new();
        public void OnGet()
        {
            visitList = VisitService.GetAll();
        }

        public string StartDateText(Visit visit)
        {
            if(visit.StartDate == null)
            {
                return "";
            }
            return visit.StartDate.ToString();
        }

        public string EndDateText(Visit visit)
        {
            if(visit.EndDate == null)
            {
                return "";
            }
            return visit.EndDate.ToString();
        }

        public IActionResult OnPostDelete(int id)
        {
            VisitService.Delete(id);
            return RedirectToAction("Get");
        }

        public IActionResult OnPostUpdate(Visit visit)
        {
            return RedirectToPage("/ModifyVisit", new { visitId = visit.Id });
        }
    }
}
