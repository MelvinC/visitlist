using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using VisitList.Models;
using VisitList.Services;

namespace Visits.Pages
{
    public class ModifyVisitModel : PageModel
    {
        public Visit visit = new();

        public void OnGet()
        {
            int id = int.Parse(HttpContext.Request.Query["visitId"]);
            Visit? temp = VisitService.Get(id);
            if(temp != null)
            {
                visit = temp;
            }
        }

        public IActionResult OnPostModifyVisit(int id)
        {
            Visit? visit = VisitService.Get(id);
            if(visit == null)
            {
                return Page();
            }

            return RedirectToAction("Get");
        }

        public IActionResult OnPostUpdate()
        {
            Visit visit = new();
            visit.Id = int.Parse(Request.Form["Id"]);
            visit.Name = Request.Form["Name"];
            
            if(Date.isValid(Request.Form["StartDate"]))
            {
                if(visit.StartDate == null)
                {
                    visit.StartDate = new();
                }
                
                visit.StartDate.year = Date.getYearFromForm(Request.Form["StartDate"]);
                visit.StartDate.month = Date.getMonthFromForm(Request.Form["StartDate"]);
                visit.StartDate.day = Date.getDayFromForm(Request.Form["StartDate"]);
            }

            if(Date.isValid(Request.Form["EndDate"]))
            {
                if(visit.EndDate == null)
                {
                    visit.EndDate = new();
                }
                visit.EndDate.year = Date.getYearFromForm(Request.Form["EndDate"]);
                visit.EndDate.month = Date.getMonthFromForm(Request.Form["EndDate"]);
                visit.EndDate.day = Date.getDayFromForm(Request.Form["EndDate"]);
            }

            visit.Comment = Request.Form["Comment"];
            VisitService.Update(visit);
            return Redirect("/Visits");
        }

         public IActionResult OnGetDeletePhoto(int visitId, string photo)
        {
            VisitService.DeletePhoto(visitId, photo);
            return RedirectToAction("Get", new { visitId = visitId });
        }

        public IFormFile? UploadPhoto { get; set; }

        public IActionResult OnPostAddPhoto(int visitId)
        {
            Visit? visitToAdd = VisitService.Get(visitId);
            if(visitToAdd != null && UploadPhoto != null)
            {
                int id = 0;
                foreach(var photo in visitToAdd.PhotoList)
                {
                    id = int.Parse(photo.Split("__")[1].Split(".")[0]);
                }
                ++id;
                var fileName = visitToAdd.Name + "__" + id + System.IO.Path.GetExtension(UploadPhoto.FileName);
                var file = Path.Combine("wwwRoot\\Photos", fileName);
                using (var fileStream = new FileStream(file, FileMode.Create))
                {
                    UploadPhoto.CopyTo(fileStream);
                }

                VisitService.AddPhoto(int.Parse(Request.Form["Id"]), fileName);
            }
            return RedirectToAction("Get", new { visitId = Request.Form["Id"] });
        }
    }
}
