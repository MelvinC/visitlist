using System.ComponentModel.DataAnnotations;

namespace VisitList.Models;

public class Date
{
    [Range(1, 31)]
    public int day { get; set; }

    [Range(1, 12)]
    public int month { get; set; }

    [Range(1900, 2100)]
    public int year { get; set; }

    public override string ToString()
    {
        string dayToDisplay = day < 10 ? "0" + day : day.ToString();
        string monthToDisplay = month < 10 ? "0" + month : month.ToString();
        return dayToDisplay + "/" + monthToDisplay + "/" + year;
    }

    public string toInputDisplay()
    {
        string dayToDisplay = day < 10 ? "0" + day : day.ToString();
        string monthToDisplay = month < 10 ? "0" + month : month.ToString();
        return  year + "-" + monthToDisplay + "-" + dayToDisplay;
    }

    public static bool isValid(string date)
    {
        var splittedDate = date.Split("-");
        return date.Split("-").Count() == 3 && splittedDate[0].Count() == 4 && splittedDate[1].Count() == 2 && splittedDate[2].Count() == 2;
    }

    public static int getYearFromForm(string date)
    {
        return int.Parse(date.Split("-")[0]);
    }

    public static int getMonthFromForm(string date)
    {
        return int.Parse(date.Split("-")[1]);
    }

    public static int getDayFromForm(string date)
    {
        return int.Parse(date.Split("-")[2]);
    }
}