using System.ComponentModel.DataAnnotations;

namespace VisitList.Models;

public class Visit
{
    public int Id { get; set; }

    [Required]
    public string? Name { get; set; }

    public Date? StartDate {get; set;}
    public Date? EndDate {get; set;}

    public string? Comment {get; set;}

    public List<string> PhotoList {get; set;} = new();

    public string getStartDateDisplay()
    {
        return StartDate != null ? StartDate.toInputDisplay() : "";
    }

    public string getEndDateDisplay()
    {
        return EndDate != null ? EndDate.toInputDisplay() : "";
    }
}