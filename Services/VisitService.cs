using VisitList.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace VisitList.Services;
public static class VisitService
{
    static List<Visit> VisitList { get; } = new();

    static string jsonFile = "visits.json";
    static int nextId = 3;
    static VisitService()
    {        
        var foundFiles = Directory.EnumerateFiles(Directory.GetCurrentDirectory(), "visits.json");
        if(foundFiles != null && foundFiles.Count() > 0)
        {
            jsonFile = foundFiles.ElementAt(0);
        }
        
        addVisit(jsonFile);
    }

    public static void addVisit(string file)
    {
        string visitJson = File.ReadAllText(file);
        List<Visit>? visit = JsonConvert.DeserializeObject<List<Visit>>(visitJson);

        if(visit != null)
        {
            VisitList.AddRange(visit);
        }
    }

    public static List<Visit> GetAll() => VisitList;

    public static Visit? Get(int id) => VisitList.FirstOrDefault(v => v.Id == id);

    public static Visit? Get(string name) => VisitList.FirstOrDefault(v => v.Name == name);

    public static void Add(Visit visit)
    {
        visit.Id = nextId++;
        VisitList.Add(visit);
    }

    public static void Delete(int id)
    {
        var visit = Get(id);
        if (visit is null)
            return;

        VisitList.Remove(visit);
    }

    public static int GetJsonIdFromVisitId(int id)
    {
        var visitListObj = JArray.Parse(File.ReadAllText(jsonFile));
        int jsonIndex = 0;
        while(jsonIndex < visitListObj.Count())
        {
            var token = visitListObj[jsonIndex]["Id"];
            if(token != null && token.Value<int>() == id)
            {
                break;
            }
            ++jsonIndex;
        }
        return jsonIndex;
    }

    public static void Update(Visit visit)
    {
        var index = VisitList.FindIndex(v => v.Id == visit.Id);
        if (index == -1)
            return;

        var visitListObj = JArray.Parse(File.ReadAllText(jsonFile));
        int jsonIndex = GetJsonIdFromVisitId(visit.Id);

        if(jsonIndex < visitListObj.Count())
        {
            visitListObj[jsonIndex]["Name"] = visit.Name;

            var startDate = visitListObj[jsonIndex]["StartDate"];
            if(startDate != null && visit.StartDate != null)
            {
                startDate["day"] = visit.StartDate.day;
                startDate["month"] = visit.StartDate.month;
                startDate["year"] = visit.StartDate.year;
            }
            else
            {
                visit.StartDate = VisitList[index].StartDate;
            }

            var endDate = visitListObj[jsonIndex]["EndDate"];
            if(endDate != null && visit.EndDate != null)
            {
                endDate["day"] = visit.EndDate.day;
                endDate["month"] = visit.EndDate.month;
                endDate["year"] = visit.EndDate.year;
            }
            else
            {
                visit.EndDate = VisitList[index].EndDate;
            }

            visitListObj[jsonIndex]["Comment"] = visit.Comment;

            visit.PhotoList = VisitList[index].PhotoList;

            string output = Newtonsoft.Json.JsonConvert.SerializeObject(visitListObj, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText("visits.json", output);

            VisitList[index] = visit;
        }
    }

    public static void DeletePhoto(int visitId, string photo)
    {
        var index = VisitList.FindIndex(v => v.Id == visitId);
        if (index == -1)
            return;

        Visit visit = VisitList[index];
        visit.PhotoList.Remove(photo);

        var visitListObj = JArray.Parse(File.ReadAllText(jsonFile));
        int jsonIndex = GetJsonIdFromVisitId(visitId);
        var jsonPhotoList = visitListObj[jsonIndex]["PhotoList"];
        if(jsonPhotoList != null && jsonPhotoList.Count() > 0)
        {
            List<JToken> photoToRemoveList = new();
            foreach(var jsonPhoto in jsonPhotoList)
            {
                var jsonPhotoLink = jsonPhoto.Value<string>();
                if(jsonPhotoLink != null && jsonPhotoLink.Equals(photo))
                {
                    photoToRemoveList.Add(jsonPhoto);
                }
            }
            foreach (var photoToRemove in photoToRemoveList)
            {
                photoToRemove.Remove();
            }
        }

        string output = Newtonsoft.Json.JsonConvert.SerializeObject(visitListObj, Newtonsoft.Json.Formatting.Indented);
        File.WriteAllText("visits.json", output);

        string photoFolder = "wwwRoot\\Photos";
        if (File.Exists(Path.Combine(photoFolder, photo)))    
        {    
            File.Delete(Path.Combine(photoFolder, photo));
        }
    }

    public static void AddPhoto(int visitId, string photoName)
    {
        var index = VisitList.FindIndex(v => v.Id == visitId);
        if (index == -1)
            return;

        Visit visit = VisitList[index];
        visit.PhotoList.Add(photoName);

        var visitListObj = JArray.Parse(File.ReadAllText(jsonFile));
        int jsonIndex = GetJsonIdFromVisitId(visit.Id);
        JArray jsonPhotoList = new();
        var oldPhotoList = visitListObj[jsonIndex]["PhotoList"];
        if(oldPhotoList != null)
        {
            foreach (var photo in oldPhotoList)
            {
                jsonPhotoList.Add(photo);
            }
        }
        jsonPhotoList.Add(photoName);
        visitListObj[jsonIndex]["PhotoList"] = jsonPhotoList;

        string output = Newtonsoft.Json.JsonConvert.SerializeObject(visitListObj, Newtonsoft.Json.Formatting.Indented);
        File.WriteAllText("visits.json", output);
    }
}